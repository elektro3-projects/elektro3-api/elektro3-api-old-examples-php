<?php

// An example on how to use the Elektro3 API to get a product

header('Content-type: text/plain; charset=utf-8');
include 'config.php';
include 'ApiClient.php';

$api = new Elektro3\ApiClient([
	'clientId' => API_CLIENT_ID,
	'clientSecret' => API_CLIENT_SECRET,
	'username' => API_USERNAME,
	'password' => API_PASSWORD
]);

try {

	// Perform the desired API request
	$result = $api->query(
		'/api/get-producto',
		[
			'product_code' => '85807',
			'iso_code' => 'es'
		]
	);

	// Check for errors in the request
	if ($result->status == 0)
		throw new Exception('Error requesting the API');
	else
	if ($result->status == 2)
		throw new Exception('Missing parameters');

}
catch (Exception $e) {
	echo $e->getMessage()."\n";
	die;
}

var_dump($result->productos[0]);
