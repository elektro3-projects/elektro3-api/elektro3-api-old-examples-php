<html>
<head>
	<title>Elektro3 API testing environment</title>
	<style>
		@import url('https://fonts.googleapis.com/css2?family=Inconsolata:wght@400,800&display=swap');
		body {
			background: #222;
			color: #fff;
			font-family: 'Inconsolata', monospace;
		}
		a {
			color: #fff;
		}
		#main {
			display: flex;
			width: 100vw;
			height: 100vh;
			flex-direction: column;
			justify-content: center;
			align-items: center;
			gap: 10px;
		}
		#main > hgroup > h1 {
			font-weight: bold;
			font-size: 20pt;
			background: #ffed3d;
			color: #000;
			padding: .5em;
			border-radius: 4px;
			text-align: center;
		}
		#main > hgroup > h2 { font-size: 14pt; }
		#main > iframe.test {
			border: 0;
			background: #fff;
			border-radius: 4px;
			width: 90%;
			height: 20vh;
			min-height: 300px;
			max-width: 1200px;
		}
		.button {
			text-decoration: none;
		}
		.button:before {
			content: '▸ ';
		}
		.button:hover {
			opacity: .8;
		}
	</style>
</head>
<body>
	<div id="main">
		<hgroup>
			<h1>Elektro3</h1>
			<h2>API testing environment v1.1</h2>
		</hgroup>
		<iframe class="test" src="./test.php"></iframe>
		<aside><a href="https://gitlab.com/elektro3-projects/elektro3-api/elektro3-api-old-examples-php" target="_newwindow" class="button">Elektro3 API examples and testing environment official repository</a></aside>
		<aside><a href="https://elektro3.com/productos/integracion-ecommerce" target="_newwindow" class="button">Elektro3 API manual and documentation</a></aside>
	</div>
</body>
</html>
