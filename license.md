# Elektro3 API Client

This software is licensed exclusively to Elektro3 clients as long as they
retain their client condition, cannot be modified nor distributed, and can
only be used for the purpose of accessing the Elektro3 API.

This software is provided as-is, without any warranty, and should be used
only by accredited professionals. Elektro3 is not held liable for any
damages caused by the misuse, faulty implementation or any other use of this
software.
